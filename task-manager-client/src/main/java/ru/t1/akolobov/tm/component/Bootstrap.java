package ru.t1.akolobov.tm.component;

import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.api.service.ILoggerService;
import ru.t1.akolobov.tm.event.ConsoleEvent;
import ru.t1.akolobov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.akolobov.tm.listener.AbstractListener;
import ru.t1.akolobov.tm.util.SystemUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Scanner;

@Setter
@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private AbstractListener[] abstractListeners;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @SneakyThrows
    private void initPid() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void prepareStartup() {
        initPid();
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        fileScanner.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
        fileScanner.stop();
    }

    private void processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) {
            return;
        }
        @Nullable final String arg = args[0];
        processArgument(arg);
        exit();
    }

    private void processCommands() {
        @NotNull final Scanner scanner = new Scanner(System.in);
        @Nullable String command = null;
        while (!"exit".equals(command)) {
            try {
                System.out.println("ENTER COMMAND:");
                command = scanner.nextLine();
                publisher.publishEvent(new ConsoleEvent(command));
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void processArgument(@Nullable final String arg) {
        @Nullable final AbstractListener abstractListener = getListenerByArgument(arg);
        if (abstractListener == null) throw new ArgumentNotSupportedException();
        publisher.publishEvent(new ConsoleEvent(abstractListener.getEventName()));
    }

    private AbstractListener getListenerByArgument(String arg) {
        if (arg == null) return null;
        return Arrays.stream(abstractListeners)
                .filter((listener) -> arg.equals(listener.getArgument()))
                .findFirst()
                .orElse(null);
    }

    private void exit() {
        System.exit(0);
    }

    public void run(@Nullable final String[] args) {
        processArguments(args);
        prepareStartup();
        processCommands();
    }

}
