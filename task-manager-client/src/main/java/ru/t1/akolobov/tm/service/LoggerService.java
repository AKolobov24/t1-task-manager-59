package ru.t1.akolobov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.akolobov.tm.api.service.ILoggerService;

import java.io.File;
import java.io.IOException;
import java.util.logging.*;

@Service
public final class LoggerService implements ILoggerService {

    @NotNull
    private static final String CONFIG_FILE = "/logger.properties";

    @NotNull
    private static final String COMMANDS = "COMMANDS";

    @NotNull
    private static final String LOG_DIRECTORY = "./log";

    @NotNull
    private static final String COMMANDS_FILE = LOG_DIRECTORY + "/commands.xml";

    @NotNull
    private static final String ERRORS = "ERRORS";

    @NotNull
    private static final String ERRORS_FILE = LOG_DIRECTORY + "/errors.xml";

    @NotNull
    private static final String MESSAGES = "MESSAGES";

    @NotNull
    private static final String MESSAGES_FILE = LOG_DIRECTORY + "/messages.xml";

    @NotNull
    private static final LogManager MANAGER = LogManager.getLogManager();

    @NotNull
    private static final Logger ROOT_LOGGER = Logger.getLogger("");

    @NotNull
    private static final Logger COMMAND_LOGGER = getCommandLogger();

    @NotNull
    private static final Logger ERROR_LOGGER = getErrorLogger();

    @NotNull
    private static final Logger MESSAGE_LOGGER = getMessageLogger();

    @NotNull
    private final ConsoleHandler consoleHandler = getConsoleHandler();

    {
        init();
        registry(COMMAND_LOGGER, COMMANDS_FILE, false);
        registry(ERROR_LOGGER, ERRORS_FILE, true);
        registry(MESSAGE_LOGGER, MESSAGES_FILE, true);
    }

    @NotNull
    public static Logger getCommandLogger() {
        return Logger.getLogger(COMMANDS);
    }

    @NotNull
    public static Logger getErrorLogger() {
        return Logger.getLogger(ERRORS);
    }

    @NotNull
    public static Logger getMessageLogger() {
        return Logger.getLogger(MESSAGES);
    }

    private void init() {
        try {
            MANAGER.readConfiguration(LoggerService.class.getResourceAsStream(CONFIG_FILE));
            @NotNull final File logDirectory = new File(LOG_DIRECTORY);
            if (!logDirectory.exists()) logDirectory.mkdir();
        } catch (final IOException e) {
            ROOT_LOGGER.severe(e.getMessage());
        }
    }

    @NotNull
    private ConsoleHandler getConsoleHandler() {
        @NotNull final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private void registry(
            @NotNull final Logger logger,
            @NotNull final String fileName,
            final boolean isConsole) {
        try {
            if (isConsole) logger.addHandler(consoleHandler);
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(fileName));
        } catch (final IOException e) {
            ROOT_LOGGER.severe(e.getMessage());
        }
    }

    @Override
    public void info(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        MESSAGE_LOGGER.info(message);
    }

    @Override
    public void debug(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        MESSAGE_LOGGER.fine(message);
    }

    @Override
    public void command(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        COMMAND_LOGGER.info(message);
    }

    @Override
    public void error(@Nullable final Exception e) {
        if (e == null) return;
        ERROR_LOGGER.log(Level.SEVERE, e.getClass().getSimpleName() + "\n" + e.getMessage(), e);
    }

}
