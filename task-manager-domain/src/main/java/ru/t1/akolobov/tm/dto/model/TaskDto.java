package ru.t1.akolobov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.enumerated.Status;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class TaskDto extends AbstractUserOwnedDtoModel {

    private static final long serialVersionUID = 1;

    @Column
    @NotNull
    private String name = "";

    @Column
    @Nullable
    private String description = "";

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "project_id")
    private String projectId;


    public TaskDto(@NotNull final String name) {
        this.name = name;
    }

    public TaskDto(@NotNull final String name, @NotNull final String description) {
        this.name = name;
        this.description = description;
    }

    public TaskDto(@NotNull final String name, @NotNull final Status status) {
        this.name = name;
        this.status = status;
    }

    @Override
    @NotNull
    public String toString() {
        return name + " : " + description + "(" + getId() + ")";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj instanceof TaskDto) {
            TaskDto anotherTask = (TaskDto) obj;
            return this.getId().equals(anotherTask.getId()) &&
                    this.getName().equals(anotherTask.getName()) &&
                    this.getStatus() == anotherTask.getStatus();
        }
        return false;
    }

}
