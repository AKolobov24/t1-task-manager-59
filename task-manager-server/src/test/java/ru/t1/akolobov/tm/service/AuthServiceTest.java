package ru.t1.akolobov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.akolobov.tm.api.service.IAuthService;
import ru.t1.akolobov.tm.api.service.IPropertyService;
import ru.t1.akolobov.tm.api.service.dto.ISessionDtoService;
import ru.t1.akolobov.tm.api.service.dto.IUserDtoService;
import ru.t1.akolobov.tm.configuration.ServerConfiguration;
import ru.t1.akolobov.tm.dto.model.SessionDto;
import ru.t1.akolobov.tm.exception.user.AccessDeniedException;
import ru.t1.akolobov.tm.exception.user.IncorrectLoginOrPasswordException;
import ru.t1.akolobov.tm.exception.user.LoginEmptyException;
import ru.t1.akolobov.tm.exception.user.PasswordEmptyException;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.util.HashUtil;

import static ru.t1.akolobov.tm.data.dto.TestUserDto.USER1;
import static ru.t1.akolobov.tm.data.dto.TestUserDto.USER2;

@Category(UnitCategory.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AuthServiceTest {

    @NotNull
    private final static ApplicationContext context =
            new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    @Autowired
    private static IPropertyService propertyService;
    @NotNull
    @Autowired
    private static ISessionDtoService sessionService;
    @NotNull
    @Autowired
    private static IUserDtoService userService;
    @NotNull
    @Autowired
    private static IAuthService authService;
    private static String token;

    @BeforeClass
    public static void prepareUserData() {
        USER1.setPasswordHash(HashUtil.salt(propertyService, USER1.getLogin()));
        userService.add(USER1);
    }

    @AfterClass
    public static void clearUserData() {
        userService.remove(USER1);
    }

    @Test
    public void login() {
        String token = authService.login(USER1.getLogin(), USER1.getLogin());
        Assert.assertNotNull(token);
        AuthServiceTest.token = token;
        Assert.assertNotNull(sessionService.findAll(USER1.getId()).get(0));
        Assert.assertThrows(
                LoginEmptyException.class,
                () -> authService.login("", USER1.getLogin())
        );
        Assert.assertThrows(
                PasswordEmptyException.class,
                () -> authService.login(USER1.getLogin(), "")
        );
        Assert.assertThrows(
                IncorrectLoginOrPasswordException.class,
                () -> authService.login(USER1.getLogin(), USER2.getLogin())
        );
        Assert.assertThrows(
                IncorrectLoginOrPasswordException.class,
                () -> authService.login(USER2.getLogin(), USER2.getLogin())
        );
        userService.lockUserByLogin(USER1.getLogin());
        Assert.assertThrows(
                IncorrectLoginOrPasswordException.class,
                () -> authService.login(USER1.getLogin(), USER1.getLogin())
        );
        userService.unlockUserByLogin(USER1.getLogin());
    }

    @Test
    public void logout() {
        authService.logout("");
        Assert.assertNotNull(sessionService.findAll(USER1.getId()).get(0));
        authService.logout(null);
        Assert.assertNotNull(sessionService.findAll(USER1.getId()).get(0));
        authService.logout(AuthServiceTest.token);
        Assert.assertTrue(sessionService.findAll(USER1.getId()).isEmpty());
        AuthServiceTest.token = null;
    }

    @Test
    public void validateToken() {
        Assert.assertThrows(AccessDeniedException.class, () -> authService.validateToken(AuthServiceTest.token));
        String token = authService.login(USER1.getLogin(), USER1.getLogin());
        SessionDto session = authService.validateToken(token);
        sessionService.removeById(session.getUserId(), session.getId());
        Assert.assertThrows(AccessDeniedException.class, () -> authService.validateToken(token));
    }

}
