package ru.t1.akolobov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.dto.model.ProjectDto;
import ru.t1.akolobov.tm.marker.UnitCategory;

import java.util.List;

import static ru.t1.akolobov.tm.data.dto.TestProjectDto.createProject;
import static ru.t1.akolobov.tm.data.dto.TestProjectDto.createProjectList;
import static ru.t1.akolobov.tm.data.dto.TestUserDto.*;

@Category(UnitCategory.class)
public final class ProjectDtoRepositoryTest {

    @BeforeClass
    public static void prepareConnection() {
        @NotNull final UserDtoRepository userRepository = new UserDtoRepository();
        userRepository.add(USER1);
        userRepository.add(USER2);
    }

    @AfterClass
    public static void closeConnection() {
        @NotNull final UserDtoRepository userRepository =
                new UserDtoRepository();
        userRepository.remove(USER1);
        userRepository.remove(USER2);
    }

    @After
    public void clearData() {
        @NotNull final ProjectDtoRepository repository =
                new ProjectDtoRepository();
        repository.clear(USER1.getId());
        repository.clear(USER2.getId());
    }

    @Test
    public void add() {
        @NotNull final ProjectDtoRepository repository =
                new ProjectDtoRepository();
        @NotNull final ProjectDto project = createProject(USER1_ID);
        repository.add(project);
        Assert.assertTrue(repository.findAll(USER1_ID).contains(project));
        Assert.assertEquals(1, repository.findAll(USER1_ID).size());
    }

    @Test
    public void clear() {
        @NotNull final ProjectDtoRepository repository =
                new ProjectDtoRepository();
        @NotNull final List<ProjectDto> projectList = createProjectList(USER1_ID);
        projectList.forEach(repository::add);
        long size = repository.getSize();
        repository.clear(USER1_ID);
        Assert.assertEquals(
                size - projectList.size(),
                repository.getSize().intValue()
        );
    }

    @Test
    public void existById() {
        @NotNull final ProjectDtoRepository repository =
                new ProjectDtoRepository();
        @NotNull final ProjectDto project = createProject(USER1_ID);
        repository.add(project);
        Assert.assertTrue(repository.existById(USER1_ID, project.getId()));
        Assert.assertFalse(repository.existById(USER2_ID, project.getId()));
    }

    @Test
    public void findAll() {
        @NotNull final ProjectDtoRepository repository =
                new ProjectDtoRepository();
        @NotNull final List<ProjectDto> user1ProjectList = createProjectList(USER1_ID);
        @NotNull final List<ProjectDto> user2ProjectList = createProjectList(USER2_ID);
        user1ProjectList.forEach(repository::add);
        user2ProjectList.forEach(repository::add);
        Assert.assertEquals(user1ProjectList, repository.findAll(USER1_ID));
        Assert.assertEquals(user2ProjectList, repository.findAll(USER2_ID));
    }

    @Test
    public void findOneById() {
        @NotNull final ProjectDtoRepository repository = new ProjectDtoRepository();
        @NotNull final ProjectDto project = createProject(USER1_ID);
        repository.add(project);
        @NotNull final String projectId = project.getId();
        Assert.assertEquals(project, repository.findOneById(USER1_ID, projectId));
        Assert.assertNull(repository.findOneById(USER2_ID, projectId));
    }

    @Test
    public void getSize() {
        @NotNull final ProjectDtoRepository repository = new ProjectDtoRepository();
        @NotNull final List<ProjectDto> projectList = createProjectList(USER1_ID);
        projectList.forEach(repository::add);
        Assert.assertEquals(projectList.size(), repository.getSize(USER1_ID).intValue());
        repository.add(createProject(USER1_ID));
        Assert.assertEquals((projectList.size() + 1), repository.getSize(USER1_ID).intValue());
    }

    @Test
    public void remove() {
        @NotNull final ProjectDtoRepository repository = new ProjectDtoRepository();
        createProjectList(USER1_ID).forEach(repository::add);
        @NotNull final ProjectDto project = createProject(USER1_ID);
        repository.add(project);
        Assert.assertEquals(project, repository.findOneById(USER1_ID, project.getId()));
        repository.remove(project);
        Assert.assertNull(repository.findOneById(USER1_ID, project.getId()));
    }

    @Test
    public void removeById() {
        @NotNull final ProjectDtoRepository repository = new ProjectDtoRepository();
        createProjectList(USER1_ID).forEach(repository::add);
        @NotNull final ProjectDto project = createProject(USER1_ID);
        repository.add(project);
        repository.removeById(USER1_ID, project.getId());
        Assert.assertNull(repository.findOneById(USER1_ID, project.getId()));
    }

}