package ru.t1.akolobov.tm.repository.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.akolobov.tm.api.repository.model.IRepository;
import ru.t1.akolobov.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

@Repository
@NoArgsConstructor
@AllArgsConstructor
@Scope("prototype")
public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    @NotNull
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull Collection<M> models) {
        models.forEach(this::add);
        return models;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull Collection<M> models) {
        clear();
        return add(models);
    }

    @Override
    public boolean existById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

}
