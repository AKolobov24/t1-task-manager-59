package ru.t1.akolobov.tm.component;

import liquibase.Contexts;
import liquibase.LabelExpression;
import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.api.service.IExternalLogService;
import ru.t1.akolobov.tm.api.service.ILoggerService;
import ru.t1.akolobov.tm.api.service.dto.IProjectDtoService;
import ru.t1.akolobov.tm.api.service.dto.ITaskDtoService;
import ru.t1.akolobov.tm.api.service.dto.IUserDtoService;
import ru.t1.akolobov.tm.endpoint.AbstractEndpoint;
import ru.t1.akolobov.tm.enumerated.Role;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.service.PropertyService;
import ru.t1.akolobov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Optional;

@Setter
@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private AbstractApplicationContext context;

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Autowired
    private IExternalLogService externalLogService;

    @NotNull
    @Autowired
    private IProjectDtoService projectService;

    @NotNull
    @Autowired
    private ITaskDtoService taskService;

    @NotNull
    @Autowired
    private IUserDtoService userService;

    @NotNull
    @Autowired
    private Backup backup;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private AbstractEndpoint[] abstractEndpoints;

    private void registry() {
        Arrays.stream(abstractEndpoints).forEach(this::registry);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPid() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initDemoData() {
        try {
            @NotNull final String user1Id = Optional
                    .ofNullable(userService.findByLogin("user1"))
                    .orElse(
                            userService.create(
                                    "user1",
                                    "user1",
                                    "user@mail.ru"
                            )
                    ).getId();
            @NotNull final String user2Id = Optional
                    .ofNullable(userService.findByLogin("akolobov"))
                    .orElse(
                            userService.create(
                                    "akolobov",
                                    "akolobov",
                                    Role.ADMIN)
                    ).getId();
            projectService.create(user1Id, "TEST PROJECT", Status.IN_PROGRESS);
            projectService.create(user1Id, "DEMO PROJECT", Status.COMPLETED);
            projectService.create(user2Id, "BETA PROJECT", Status.NOT_STARTED);
            projectService.create(user2Id, "BEST PROJECT", Status.IN_PROGRESS);
            taskService.create(user1Id, "TEST TASK", Status.IN_PROGRESS);
            taskService.create(user1Id, "DEMO TASK", Status.COMPLETED);
            taskService.create(user1Id, "BETA TASK", Status.NOT_STARTED);
            taskService.create(user1Id, "BEST TASK", Status.IN_PROGRESS);
        } catch (@NotNull final Exception e) {
            loggerService.info("Error while initiating demo data!");
            loggerService.error(e);
        }
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
        backup.stop();
        externalLogService.stop();
    }

    public void run() {
        initPid();
        registry();
        prepareDataBase();
        initDemoData();
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
    }

    @SneakyThrows
    private void prepareDataBase() {
        try (Liquibase liquibase = context.getBean(Liquibase.class)) {
            liquibase.update(new Contexts(), new LabelExpression());
        } catch (LiquibaseException e) {
            throw new RuntimeException(e);
        }
    }

}
