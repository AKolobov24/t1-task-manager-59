package ru.t1.akolobov.tm.dto;


import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.enumerated.OperationType;

@Getter
@Setter
public class OperationEvent {

    @NotNull
    private final OperationType type;

    @NotNull
    private final Object entity;

    @Nullable
    private String table;

    private final long timestamp = System.currentTimeMillis();

    public OperationEvent(@NotNull final OperationType type, @NotNull final Object entity) {
        this.type = type;
        this.entity = entity;
    }

}
