package ru.t1.akolobov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.dto.model.UserDto;

public interface IUserDtoRepository extends IDtoRepository<UserDto> {

    @Nullable
    UserDto findByLogin(@NotNull String login);

    @Nullable
    UserDto findByEmail(@NotNull String email);

    boolean isLoginExist(@NotNull String login);

    boolean isEmailExist(@NotNull String email);

}
